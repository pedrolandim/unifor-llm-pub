FROM ubuntu:22.04

WORKDIR /usr/src/app

RUN apt-get update --fix-missing

RUN apt-get install git -y

RUN apt-get install python3 -y \
    && apt-get install python3-pip -y

COPY requirements.txt ./
RUN pip3 install --no-cache-dir -r requirements.txt

RUN pip3 install --force --no-deps git+https://github.com/UKPLab/sentence-transformers.git

COPY . .

EXPOSE 8501
CMD ["streamlit", "run", "app.py" ]