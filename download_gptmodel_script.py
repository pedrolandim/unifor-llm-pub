import requests
from pathlib import Path
from tqdm import tqdm


def load_gpt4all_model(model_name: str):
    print(f"Iniciando download do modelo {model_name}...")
    local_path = f'./models/{model_name}'
    Path(local_path).parent.mkdir(parents=True, exist_ok=True)

    url = 'http://gpt4all.io/models/ggml-gpt4all-l13b-snoozy.bin'

    # send a GET request to the URL to download the file. Stream since it's large
    response = requests.get(url, stream=True)

    # open the file in binary mode and write the contents of the response to it in chunks
    # This is a large file, so be prepared to wait.
    with open(local_path, 'wb') as f:
        for chunk in tqdm(response.iter_content(chunk_size=8192)):
            if chunk:
                f.write(chunk)


load_gpt4all_model("ggml-gpt4all-l13b-snoozy.bin")