import streamlit as st
from dotenv import load_dotenv
from PyPDF2 import PdfReader
from langchain.text_splitter import CharacterTextSplitter
from langchain.embeddings import HuggingFaceEmbeddings
from langchain.vectorstores import FAISS
from langchain.llms import HuggingFaceHub
from langchain.llms import GPT4All
from langchain.memory import ConversationBufferMemory
from langchain.chains import ConversationalRetrievalChain
from html_templates import css, bot_template, user_template
from mr_robot_ocr import TesseractOcrEngine

def get_pdf_text(pdf_docs):
    """
    Extrai o texto dos documentod pdf.
    Args:
        pdf_docs ([str]): Caminho absoutos dos arquivos PDF.
    """
    
    text = ""
    for pdf in pdf_docs:
        text += TesseractOcrEngine(file_path=pdf).pre_otsu_thresholding().process_document()
    return text


def get_text_chunks(raw_text):
    """
    Divide o texto em 'chunks'
    """
    text_splitter = CharacterTextSplitter(
        separator="\n",
        chunk_size=1000,
        chunk_overlap=200,
        length_function=len
    )
    chunks = text_splitter.split_text(raw_text)
    return chunks


def get_vectorstore(text_chunks):
    """
    Retorna o banco com os embeddings dos PDFs.
    """
    embeddings = HuggingFaceEmbeddings(model_name="hkunlp/instructor-xl")
    vectorstore = FAISS.from_texts(texts=text_chunks, embedding=embeddings)
    return vectorstore


def get_conversation_chain(vectorstore):
    llm = GPT4All(model="./models/ggml-gpt4all-l13b-snoozy.bin")
    memory = ConversationBufferMemory(memory_key='chat_history', return_messages=True)
    conversation_chain = ConversationalRetrievalChain.from_llm(
        llm=llm,
        retriever=vectorstore.as_retriever(),
        memory=memory
    )
    return conversation_chain


def handle_userinput(user_question):
    response = st.session_state.conversation({'question': user_question})
    st.session_state.chat_history = response['chat_history']

    for i, message in enumerate(st.session_state.chat_history):
        if i % 2 == 0:
            st.write(user_template.replace("{{MSG}}", message.content), unsafe_allow_html=True)
        else:
            st.write(bot_template.replace("{{MSG}}", message.content), unsafe_allow_html=True)


def main():
    load_dotenv()

    st.set_page_config(page_title="Unifor LLM", page_icon=":books:")
    st.write(css, unsafe_allow_html=True)

    if "conversation" not in st.session_state:
        st.session_state.conversation = None
    if "chat_history" not in st.session_state:
        st.session_state.chat_history = None

    st.header("UNIFOR LLM :books:")
    user_question = st.text_input("Pergunte sobre os documentos:")
    if user_question:
        handle_userinput(user_question)

    with st.sidebar:
        st.subheader("Seus Documentos")
        pdf_docs = st.file_uploader("Carregue seus PDFs e clique em 'Processar'", accept_multiple_files=True)
        if st.button("Processar"):
            with st.spinner("Carregando"):
                #get pdf text
                raw_text = get_pdf_text(pdf_docs)
                #get the text chunks
                text_chunks = get_text_chunks(raw_text)
                #create vector store
                vectorstore = get_vectorstore(text_chunks)

                # create conversation chain
                st.session_state.conversation = get_conversation_chain(vectorstore)


if __name__ == "__main__":
    print("Iniciando aplicação")
    main()
    # test()